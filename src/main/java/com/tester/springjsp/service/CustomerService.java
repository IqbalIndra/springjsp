package com.tester.springjsp.service;

import java.util.List;

import com.tester.springjsp.model.Customer;

public interface CustomerService {
	List<Customer> getAllCustomers();
	void addCustomer(Customer customer);
	void updateCustomer(Customer customer);
	Integer deleteCustomer(Integer id);
	Customer getCustomerById(Integer id);
}
