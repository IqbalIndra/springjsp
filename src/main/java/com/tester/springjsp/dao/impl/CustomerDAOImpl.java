package com.tester.springjsp.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.tester.springjsp.dao.CustomerDAO;
import com.tester.springjsp.model.Customer;
import com.tester.springjsp.util.BaseDao;

@Repository
public class CustomerDAOImpl extends BaseDao implements CustomerDAO{

	@SuppressWarnings("unchecked")
	@Override
	public List<Customer> getAllCustomers() {
		// TODO Auto-generated method stub
		return getCurrentSession().createCriteria(Customer.class).list();
	}

	@Override
	public void addCustomer(Customer customer) {
		// TODO Auto-generated method stub
		getCurrentSession().save(customer);
	}

	@Override
	public void updateCustomer(Customer customer) {
		// TODO Auto-generated method stub
		getCurrentSession().update(customer);
	}

	@Override
	public Integer deleteCustomer(Integer id) {
		// TODO Auto-generated method stub
		Customer customer = getCustomerById(id);
		if(customer != null){
			getCurrentSession().delete(customer);
			return 1;
		}
		
		return 0;
	}

	@Override
	public Customer getCustomerById(Integer id) {
		// TODO Auto-generated method stub
		return (Customer) getCurrentSession().get(Customer.class, id);
	}

}
