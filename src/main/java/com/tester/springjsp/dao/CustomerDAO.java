package com.tester.springjsp.dao;

import java.util.List;

import com.tester.springjsp.model.Customer;

public interface CustomerDAO {
	List<Customer> getAllCustomers();
	void addCustomer(Customer customer);
	void updateCustomer(Customer customer);
	Integer deleteCustomer(Integer id);
	Customer getCustomerById(Integer id);
}
